# ------------------------------------------------------
# Check for required packages to run the installation
# ------------------------------------------------------

echo "Checking that required packages for the installation are installed..."
echo ""
_installPackagesPacman "rsync" "gum";
echo ""